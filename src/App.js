import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  state = {
    username: "this is default input"
  }

  userInputChangeHandler = (event) => {
    this.setState({
      username: event.target.value
    });
  }

  userOutputChangeHandler = () => {
    this.setState({
      username: "Please Input new username"
    });
  }

  render() {
    return (
      <div className="App">
        <p>
          <UserInput change={this.userInputChangeHandler} />
        </p>
        <p>
          <UserOutput inputVal={this.state.username} />
          <UserOutput click={this.userOutputChangeHandler} inputVal={this.state.username} />
        </p>
      </div>
    );
  }
}

export default App;
