import React from 'react';
import './UserOutput.css';

const UserOutput = (props) => {
    return (
        <div onClick={props.click} className="userOut">
            <p >{props.inputVal}</p>
        </div>
    )
}

export default UserOutput;