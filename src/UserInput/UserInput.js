import React from 'react';
import './UserInput.css';

const userInput =(props)=>{
    
    const style={
        backgroundColor:'white',
        border:'1px solid blue',
        padding:'8px',
        cursor:'pointer'
      }

    return(
       <div>
          <p style={style}> User Name: <input type="text" onChange={props.change} value={props.userInput}/>
          </p>
       </div> 
    )
};

export default userInput;